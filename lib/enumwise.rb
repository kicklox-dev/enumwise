# frozen_string_literal: true

require 'extensions/integer.rb'

require 'enumwise/railtie'
require 'enumwise/active_record.rb'

module Enumwise
  # Your code goes here...
end
