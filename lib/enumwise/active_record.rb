# frozen_string_literal: true

module Enumwise
  module ActiveRecord
    extend ActiveSupport::Concern
    #############################
    ##   INSTANCE METHODS
    #############################
    included do
      def wise(column_name)
        wisers = wiser_for(column_name)
        values = wisers.values.reject! { |pv| ((send(column_name, true) || 0).to_i(2) & pv).zero? }.compact
        values.map! { |v| unwise(column_name, v) }
      rescue ArgumentError
        []
      end

      def wise_value(column_name, array_or_symbol)
        return '0' if !array_or_symbol || (array_or_symbol.is_a?(Array) && array_or_symbol&.empty?)

        value = 0
        Array(array_or_symbol).map { |symbol| value |= wiser_for(column_name)[symbol.to_sym] }.last.to_binary
      rescue TypeError
        raise InvalidWise, "#{(Array(array_or_symbol) - self.class.send("all_#{column_name.to_s.pluralize}")).join(', ')} is not defined in wisers list"
      end

      def add_wise(column_name, value)
        (wise_column_integer_value(column_name) | wize_force_integer_value(value)).to_binary
      end

      def remove_wise(column_name, value)
        (wise_column_integer_value(column_name) & ~wize_force_integer_value(value)).to_binary
      end

      def wise?(column_name, value)
        wise_column_integer_value(column_name) & wize_force_integer_value(wise_value(column_name, value)) != 0
      end

      def unwise(column_name, value)
        wiser_for(column_name).invert[value]
      end

      # return wiser for a column, if column is not an enumwiser return nil
      def wiser_for(column_name)
        wisers = self.class.const_get("#{column_name}_wiser".upcase)
        return wisers unless wisers.is_a?(Proc)

        self.class.build_wiser_array_or_hash(column_name, wisers: wisers.call)
      rescue NameError
        nil
      end

      def wize_force_integer_value(value)
        return value.to_i(2) if value.is_a?(String)

        value.to_i
      end

      def wise_column_integer_value(column_name)
        value = send("origin_#{column_name}")
        wize_force_integer_value(value)
      end
    end

    #############################
    ##     CLASS METHODS
    #############################
    class_methods do
      # wisers can be an array, a hash or an Proc return an Array or a hash
      def enumwise(attribute_name, wisers: nil, validations: true, singular: false)
        column_name ||= attribute_name

        build_wiser(column_name, wisers: wisers)

        cattr_accessor :wisers_attributes
        self.wisers_attributes ||= {}
        self.wisers_attributes[attribute_name] = singular ? :singular : :multiple

        # define method to get all wisers available for attribute
        define_singleton_method("all_#{attribute_name.to_s.pluralize}") do
          new.wiser_for(column_name).keys
        end

        create_origin_alias(column_name)
        define_method(attribute_name) do |suber = false|
          return send("origin_#{column_name}") if suber

          singular ? wise(column_name).first : wise(column_name)
        end

        create_origin_alias("#{attribute_name}=")
        define_method("#{attribute_name}=") do |array_or_symbol|
          raise ArgumentError, 'You pass an array for a singular enumwise. Please pass a string or a symbol' if singular && array_or_symbol.is_a?(Array)

          value = self.class.overlap_value_for_bit_type(column_name, send(:wise_value, column_name, array_or_symbol))
          send("origin_#{column_name}=", value)
        end

        define_array_method(attribute_name, column_name) unless singular
        define_valdations(attribute_name, column_name) if validations
      end

      def define_array_method(attribute_name, column_name)
        define_method("add_to_#{attribute_name}") do |symbol|
          value = self.class.overlap_value_for_bit_type(column_name, send(:add_wise, column_name, wise_value(column_name, symbol)))
          send("origin_#{column_name}=", value)
        end

        define_method("remove_to_#{attribute_name}") do |symbol|
          value = self.class.overlap_value_for_bit_type(column_name, send(:remove_wise, column_name, wise_value(column_name, symbol)))
          send("origin_#{column_name}=", value)
        end
      end

      def define_valdations(attribute_name, column_name)
        validates_presence_of column_name
        validate :"valid_wise_#{column_name}"

        define_method("valid_wise_#{column_name}") do
          arr = send(attribute_name)
          return if (Array(arr) - self.class.send("all_#{attribute_name.to_s.pluralize}")).empty?

          errors.add(attribute_name, 'is not included in the wisers list')
        end
      end

      def overlap_value_for_bit_type(column_name, value)
        column = columns.find { |s| s.name == column_name.to_s }
        if defined?(::ActiveRecord::Base) && ::ActiveRecord::Base.connection.adapter_name == 'PostgreSQL' && column.type == :bit
          laps = column.limit
          return value.rjust(laps, '0')
        end
        value
      end

      def create_origin_alias(name)
        new.attributes
        alias_method :"origin_#{name}", name.to_sym
      rescue 
        define_singleton_method(name.to_sym) do
          Rails.logger.warn("#{name} is not a valid columns")
        end
      end

      # construct power of 2 value for each elements of wisers for column_name
      # construct a CONSTANT like #{column_name}_WISER, contain wiser
      def build_wiser(column_name, wisers:)
        wiser = wisers if wisers.is_a?(Proc)
        wiser ||= build_wiser_array_or_hash(column_name, wisers)

        const_set("#{column_name}_wiser".upcase, wiser) unless const_defined?("#{column_name}_wiser".upcase)
      end

      def build_wiser_array_or_hash(column_name, wisers:)
        {}.tap do |hash|
          if wisers.is_a?(Array)
            wisers.each_with_index { |value, i| hash[value] = 2**i }
          elsif wisers.is_a?(Hash)
            validate_value!(column_name, hash: wisers)
            hash.merge!(wisers.sort_by { |_k, v| v }.to_h)
          end
          hash[:empty] = 0
        end
      end

      # Validate value for hash passing in wisers
      # Test is value is a int and a power of 2
      def validate_value!(column_name, hash:)
        if invalid_value = hash.find { |_, value| (Math.log2(value) % 1.0) != 0 }
          raise ArgumentError, "#{column_name} value should be a power of 2, but #{invalid_value.first} isn't"
        end
      end
    end

    class WiserNotImplemented < StandardError; end
    class InvalidWise < StandardError; end
  end
end

ActiveRecord::Base.send(:include, Enumwise::ActiveRecord)
