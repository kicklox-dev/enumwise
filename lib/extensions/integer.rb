class Integer
  def to_hex
    to_s(16)
  end

  def to_binary(padding: 0)
    to_s(2).rjust(padding, '0')
  end
end