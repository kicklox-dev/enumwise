# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'enumwise/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'enumwise'
  s.version     = Enumwise::VERSION
  s.authors     = ['Kicklox']
  s.email       = ['contact@kicklox.com']
  s.homepage    = 'https://gitlab.com/kicklox-dev/enumwise'
  s.summary     = 'Bitwise enum for Rails model'
  s.description = 'Description of Enumwise.'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'rails', '~> 5.2.0'
end
